# WiiW8Me
A Java program that transforms a Wii Balance Board into a scale and uploads the measurement into a Google Docs. The program uses the [WiiRemoteJ](https://code.google.com/p/bochovj/wiki/WiiRemoteJ) library to read values from the board.

The program was only tested on a MacBook Pro with Java 7 and OS X 10.9.1. It should run on other platforms as well, but it will require a recompiling of the BlueCove library. See the "Problems encountered section" for more information.

## Requirements
* [BlueCove](http://bluecove.org/) a Java library for Bluetooth (JSR-82 implementation)
* Java 7
* Google account (you can easily replace this part if you don't want to use it)

## Google Docs upload
To upload your weight in a Google Docs, you need to add a `gdocs.properties` file to the `src` directory. The properties are the following:

```
username=YOUR_USERNAME
password=PASSWORD (If you use 2-factor authentication then an application specific password is requried)
spreadsheet=SPREADSHEET_NAME (e.g. My Weight)
worksheet=WORKSHEET_NAME (e.g. Sheet1)
```

## Problems encountered
### BlueCove
I ran into the following error when trying to use the original BlueCove 2.1.0 version under OS X 10.9.1 with Java 1.7.

```
dyld: lazy symbol binding failed: Symbol not found: _IOBluetoothLocalDeviceReadSupportedFeatures
  Referenced from: /private/var/folders/c7/yng7lqm93sg1lm_ytn08w7qw0000gn/T/bluecove_mateu_0/libbluecove.jnilib
  Expected in: /System/Library/Frameworks/IOBluetooth.framework/Versions/A/IOBluetooth

dyld: Symbol not found: _IOBluetoothLocalDeviceReadSupportedFeatures
  Referenced from: /private/var/folders/c7/yng7lqm93sg1lm_ytn08w7qw0000gn/T/bluecove_mateu_0/libbluecove.jnilib
  Expected in: /System/Library/Frameworks/IOBluetooth.framework/Versions/A/IOBluetooth
```

I checked out the BlueCove fork from: `https://github.com/ma-ku/bluecove` and also had to install *Xcode 5* as there was a problem with “lipo” command when trying to build the source.

With this I was able to build the BlueCove 2.1.1 SNAPSHOT.
### Wii Balance Board
I wasn't able to pair the board permanently with my MacBook. Therefore I still need to flip the board over to push the red sync button before I can weight me. Otherwise a push on the front button of the board should be enough to start the measurement.