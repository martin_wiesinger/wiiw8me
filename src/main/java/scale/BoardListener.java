package scale;


import java.util.ArrayList;
import java.util.List;

import wiiremotej.event.BBMassEvent;
import wiiremotej.event.BalanceBoardAdapter;

public class BoardListener extends BalanceBoardAdapter
{
    private List<Double> _measures;
    
    private boolean _started;
    
    private WiiW8Me _w8me;
    
    public BoardListener(WiiW8Me w8me)
    {
        _measures = new ArrayList<Double>();
        _w8me = w8me;
    }
    
    public void massInputReceived(BBMassEvent evt)
    {
        double mass = evt.getTotalMass();
        if (mass > 30)
        {
            if (!_started)
            {
                System.out.println("Start measurment");
                _started = true;
            }
            _measures.add(mass);
        } else if (mass < 30 && _started)
        {
            System.out.println("Finished measurement");
            _started = false;
            Statistics stats = new Statistics(this.getMeasures());
            _w8me.setWeight(stats.getMedian());
        } 
    }
 
    private double[] getMeasures()
    {
        double[] values = new double[_measures.size()];
        for (int i = 0; i < _measures.size(); i++)
            values[i] = _measures.get(i);
        return values;
    }
}
