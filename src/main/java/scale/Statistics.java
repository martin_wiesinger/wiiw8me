package scale;


import java.util.Arrays;

public class Statistics
{
    double[] _data;
    double _size;
    int _precision;

    public Statistics(double[] data)
    {
       this(data, 2);
    }

    public Statistics(double[] data, int precision)
    {
        _size = data.length;
        _precision = precision;
        _data = data;
        Arrays.sort(_data);
    }

    private double roundHalfToEven(double value)
    {
        double factor = Math.pow(10, _precision);
        return Math.rint(value * factor) / factor;
    }

    public double getMean()
    {
        double sum = 0.0;
        for (double a : _data)
            sum += a;
        return roundHalfToEven(sum / _size);
    }

    public double getVariance()
    {
        double mean = getMean();
        double temp = 0;
        for (double a : _data)
            temp += (mean - a) * (mean - a);
        return roundHalfToEven(temp / (_size - 1));
    }

    public double getStdDev()
    {
        return roundHalfToEven(Math.sqrt(getVariance()));
    }

    public double getMedian()
    {
        double res = 0.0;

        if (_data.length % 2 == 0)
        {
            res = (_data[(_data.length / 2) - 1] + _data[_data.length / 2]) / 2.0;
        }
        else
        {
            res = _data[_data.length / 2];
        }
        return roundHalfToEven(res);
    }

    public double getMax()
    {
        return _data[_data.length - 1];
    }

    public double getMin()
    {
        return _data[0];
    }
}
