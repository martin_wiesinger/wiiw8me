package scale.service;

public interface ProcessingService
{
    public void processValues(String... values) throws Exception;
}
