package scale.service;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.client.spreadsheet.WorksheetQuery;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;

public class GDocsService implements ProcessingService
{

    private static final String PROPERTY_FILE = "/gdocs.properties";

    private static final String USERNAME_PROP = "username";

    private static final String PASSWORD_PROP = "password";

    private static final String SPREADSHEET_PROP = "spreadsheet";

    private static final String WORKSHEET_PROP = "worksheet";

    private SpreadsheetService _service;

    private FeedURLFactory _feedURLFactory;

    private Properties _properties;

    public GDocsService() throws Exception
    {
        _feedURLFactory = FeedURLFactory.getDefault();
        _service = new SpreadsheetService("W8me-data-upload");
        InputStream input = getClass().getResourceAsStream(PROPERTY_FILE);
        _properties = new Properties();
        _properties.load(input);

        _service.setUserCredentials(_properties.getProperty(USERNAME_PROP),
                _properties.getProperty(PASSWORD_PROP));
    }

    /**
     * Gets the SpreadsheetEntry for the first spreadsheet with that name
     * retrieved in the feed.
     * 
     * @param spreadsheet the name of the spreadsheet
     * @return the first SpreadsheetEntry in the returned feed, so latest
     *         spreadsheet with the specified name
     * @throws Exception if error is encountered, such as no spreadsheets with
     *             the
     *             name
     */
    private SpreadsheetEntry getSpreadsheet(String spreadsheet)
            throws Exception
    {

        SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(
                _feedURLFactory.getSpreadsheetsFeedUrl());
        spreadsheetQuery.setTitleQuery(spreadsheet);
        SpreadsheetFeed spreadsheetFeed = _service.query(spreadsheetQuery,
                SpreadsheetFeed.class);
        List<SpreadsheetEntry> spreadsheets = spreadsheetFeed.getEntries();
        if (spreadsheets.isEmpty())
        {
            throw new Exception("No spreadsheets with that name");
        }

        return spreadsheets.get(0);
    }

    /**
     * Get the WorksheetEntry for the worksheet in the spreadsheet with the
     * specified name.
     * 
     * @param spreadsheet the name of the spreadsheet
     * @param worksheet the name of the worksheet in the spreadsheet
     * @return worksheet with the specified name in the spreadsheet with the
     *         specified name
     * @throws Exception if error is encountered, such as no spreadsheets with
     *             the
     *             name, or no worksheet wiht the name in the spreadsheet
     */
    private WorksheetEntry getWorksheet(String spreadsheet, String worksheet)
            throws Exception
    {

        SpreadsheetEntry spreadsheetEntry = getSpreadsheet(spreadsheet);

        WorksheetQuery worksheetQuery = new WorksheetQuery(
                spreadsheetEntry.getWorksheetFeedUrl());

        worksheetQuery.setTitleQuery(worksheet);
        WorksheetFeed worksheetFeed = _service.query(worksheetQuery,
                WorksheetFeed.class);
        List<WorksheetEntry> worksheets = worksheetFeed.getEntries();
        if (worksheets.isEmpty())
        {
            throw new Exception("No worksheets with that name in spreadhsheet "
                    + spreadsheetEntry.getTitle().getPlainText());
        }

        return worksheets.get(0);
    }

    /**
     * Find the index of the first empty row in the workseet based on the column
     * number.
     * 
     * @param column number of the cell that should be checked if it is empty.
     *            The first column has index 1.
     * @return index of the empty row
     * @throws Exception if error is encountered, such as bad permissions
     */
    public int findEmptyRowIndex(int column)
            throws Exception
    {
        String spreadsheet = _properties.getProperty(SPREADSHEET_PROP);
        String worksheet = _properties.getProperty(WORKSHEET_PROP);

        WorksheetEntry worksheetEntry = getWorksheet(spreadsheet, worksheet);

        int rowIndex = 1;
        ListFeed listFeed = _service.getFeed(worksheetEntry.getListFeedUrl(),
                ListFeed.class);
        List<ListEntry> rows = listFeed.getEntries();
        boolean empty = false;
        for (ListEntry row : rows)
        {
            empty = true;
            if (column == 1 && !row.getTitle().getPlainText().isEmpty())
            {
                empty = false;
            } else if (column > 1)
            {
                Set<String> tags = row.getCustomElements().getTags();
                if (tags.size() < column)
                    throw new IllegalArgumentException(
                            "Column index out of bounce");

                int i = 1;
                for (String tag : tags)
                {
                    if (i == column
                            && !row.getCustomElements().getValue(tag).isEmpty())
                    {
                        empty = false;
                    }
                }
            } else
            {
                throw new IllegalArgumentException(
                        "Column with index 0 does not exist");
            }

            if (empty)
                break;
            else
                rowIndex++;
        }
        return rowIndex + 1;
    }

    /**
     * Inserts a cell entry in the worksheet.
     * 
     * @param spreadsheet the name of the spreadsheet
     * @param worksheet the name of the worksheet
     * @param row the index of the row
     * @param column the index of the column
     * @param input the input string for the cell
     * @throws Exception if error is encountered, such as bad permissions
     */
    private void insertCellEntry(String spreadsheet, String worksheet,
            int row, int column, String input) throws Exception
    {

        URL cellFeedUrl = getWorksheet(spreadsheet, worksheet).getCellFeedUrl();

        CellEntry newEntry = new CellEntry(row, column, input);

        _service.insert(cellFeedUrl, newEntry);
    }

    /**
     * Save values to the spreadsheet at the row provided.
     * 
     * @param row index at which the data should be inserted. The first row
     *            starts with index 1.
     * @param values one entry per column
     * @throws Exception if error is encountered, such as bad permissions or
     *             missing spreadsheet
     */
    public void uploadValues(int row, String... values) throws Exception
    {
        String spreadsheet = _properties.getProperty(SPREADSHEET_PROP);
        String worksheet = _properties.getProperty(WORKSHEET_PROP);

        for (int i = 0; i < values.length; i++)
        {
            insertCellEntry(spreadsheet, worksheet, row, i + 1, values[i]);
        }
    }

    @Override
    public void processValues(String... values) throws Exception
    {
        System.out.println("Save data to Google Docs ["
                + _properties.getProperty(SPREADSHEET_PROP) + "]");
        GDocsService client = new GDocsService();
        int lastRow = client.findEmptyRowIndex(1);
        System.out.println(Arrays.toString(values));
        client.uploadValues(lastRow, values);
    }

    public static void main(String[] args) throws Exception
    {
        GDocsService client = new GDocsService();
        int lastRow = client.findEmptyRowIndex(1);

        String[] values = new String[] {
                new SimpleDateFormat("yyyy-MM-dd hh:mm a").format(new Date()),
                Double.toString(99.78) };
        client.uploadValues(lastRow, values);
    }

}
