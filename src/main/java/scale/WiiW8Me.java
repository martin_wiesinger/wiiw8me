package scale;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.intel.bluetooth.BlueCoveConfigProperties;

import scale.service.GDocsService;
import scale.service.ProcessingService;
import wiiremotej.BalanceBoard;
import wiiremotej.WiiRemoteJ;

public class WiiW8Me
{

    private Double _weight;

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // init bluetooth
        System.setProperty(
                BlueCoveConfigProperties.PROPERTY_JSR_82_PSM_MINIMUM_OFF,
                "true");
        // disable logging
        WiiRemoteJ.setConsoleLoggingOff();

        BalanceBoard board = null;
        int tries = 0;
        if (args.length == 0)
        {
            System.out.println("Press the red sync button on your board");
            while (board == null && tries < 3)
                // try to find a close by Wii Balance Board
                try
                {
                    board = WiiRemoteJ.findBalanceBoard();
                } catch (IOException e)
                {
                    // connection error, try again
                    System.out.println("Connection error, try again");
                    tries++;
                } catch (Exception e)
                {
                    e.printStackTrace();
                    System.exit(0);
                }
        } else
        {
            // expect the MAC address as the first argument
            try
            {
                board = WiiRemoteJ.connectToBalanceBoard(args[0]);
            } catch (IOException e)
            {
                e.printStackTrace();
                return;
            }
        }
        new WiiW8Me(board);
    }

    public WiiW8Me(BalanceBoard board)
    {
        System.out.println("Trying to connect...");
        try
        {
            BoardListener listener = new BoardListener(this);
            board.addBalanceBoardListener(listener);
            // illuminate board LED to show that the board is ready
            System.out.println("Step on the board");
            board.setLEDIlluminated(true);
            while (_weight == null)
            {
                Thread.sleep(1000l);
            }
            // save weight in a Google Docs spreadsheet
            String[] values = new String[] {
                    new SimpleDateFormat("yyyy-MM-dd hh:mm a")
                            .format(new Date()),
                    Double.toString(_weight) };
            ProcessingService service = new GDocsService();
            service.processValues(values);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("Disconnect board");
        board.disconnect();
    }

    public void setWeight(Double weight)
    {
        _weight = weight;
    }

}
